//[SECTION] Dependencies and Modules
    const Product = require('../models/Product');
    const bcrypt = require('bcrypt');
    const auth = require('../auth')

//[SECTION] Functionalities [CREATE]
    module.exports.createProduct = (adminInput) => {
        let pName = adminInput.name;
        let pDesc = adminInput.description;
        let price = adminInput.price;
        let pPic = adminInput.thumbnail;

         let newProduct = new Product ({
            name: pName,
            description: pDesc,
            price: price,
            thumbnail: pPic
        });

        return newProduct.save().then((product, err)=> {
            if (product) {
            return true;
            } else {
                return false;
            };
        });
    };

//[SECTION] Functionalities [RETRIEVE]

    module.exports.getAllProducts = () => {
        return Product.find({}).then(result => {
            return result;
        });
    }

    module.exports.getActiveProducts = () => {
         return Product.find({isActive: true}).then(results=>{
             return results
         })
    }

    module.exports.productOrders = (id) => {
        return Product.findById(id).then(product => {
            return product.orders
        })
    }

    module.exports.singleProduct = (id) => {
        return Product.findById(id).then(product => {
            return product
        })
    }



//[SECTION] Functionality [Update]

    module.exports.updateProduct = (product, details) => {
        let pName = details.name;
		let pDesc = details.description;
		let pCost = details.price;
        let pPic = details.thumbnail;
        let pStat = details.isActive;

		let updatedProduct = {		
			name: pName,
			description: pDesc,
			price: pCost,
            thumbnail: pPic,
            isActive: pStat
		}; 

		let id = product.productId;
		return Product.findByIdAndUpdate(id, updatedProduct).then((productUpdated, err) => {
			if (productUpdated) {
				return true; 
			} else {
				return false;
			};
		}); 

    };

    module.exports.archiveProduct = (product) => {
	    let id = product.productId; 
	    let updates = {
	   	  isActive: false 
	    } 
		return Product.findByIdAndUpdate(id, updates).then((archived, err) => {
			if (archived) {
				return 'Product archived'; 
			} else {
				return false; 
			}
		});
	};


