const bcrypt = require('bcrypt');
const auth = require('../auth');
const Service = require('../models/Service');


 module.exports.registerClient = (adminInput) => {
        let cName = adminInput.name;
        let cLoc = adminInput.location;
        let sName = adminInput.serviceName;
        let sType = adminInput.serviceType;

         let newService = new Service ({
            client: cName,
            location: cLoc,
            service: [{serviceName: sName, serviceType: sType}]
        });

        return newService.save().then((service, err)=> {
            if (service) {
            return `Service ${sName} Successfully Created!`;
            } else {
                return false;
            };
        });
    };
