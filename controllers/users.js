//[SECTION] Dependencies and Modules
    const User = require('../models/User');
    const bcrypt = require('bcrypt');
    const auth = require('../auth')
    const Product = require('../models/Product')
    const Order = require('../models/Order');
const res = require('express/lib/response');

//[SECTION] Functionalities [CREATE]
    module.exports.registerUser = (userInput) => {
        let fName = userInput.firstName;
        let lName = userInput.lastName;
        let email = userInput.email;
        let passW = userInput.password;
        let phone = userInput.mobile;

         let newUser = new User ({
            firstName: fName,
            lastName: lName,
            email: email,
            mobile: phone,
            password: bcrypt.hashSync(passW, 10),
        });

        return newUser.save().then((user, err)=> {
            if (user) {
                return user;
            } else {
                return false;
            };
        });
    };

    module.exports.loginUser = (reqBody) => {
        let uEmail = reqBody.email;
        let uPassw = reqBody.password;
        return User.findOne({email: uEmail}).then(result => {
            if (result === null) {
                return false
            } else {    
                const isMatched = bcrypt.compareSync(uPassw, result.password)
                if (isMatched) {
                        let userData = result.toObject();
                        return{accessToken: auth.createAccessToken(userData)}
                } else {
                    return false
                }
            }
        });  
    }

    module.exports.order = async (data) => {
        let id = data.userId;
        let product = data.productId;
        let orders = data.orders;
        let uSize = data.size;
        let uColor = data.color;
        let orderNum = (+new Date).toString(36).slice(-7);

        let itemPrice = await Product.findById(product).then(item => item.price)

        let subTotal = itemPrice * orders;

        console.log(itemPrice)
        console.log(orders)
        console.log(subTotal)

        let isUserUpdated = User.findById(id).then(user => {
            user.orders.push({orderId: orderNum,productId: product,color: uColor, size: uSize, quantity: orders, subTotal: subTotal, price: itemPrice});
          
            return user.save().then((save, error) => {
                        if (error) {
                            return false;
                        } else {
                            return true; 
                        }
                    });
        })

        let isProductUpdated = Product.findById(product).then(product => {
            product.orders.push({userId: id,orderId: orderNum,quantity: orders, color: uColor, size: uSize});
            return product.save().then((saved, err) => {
                if (err) {
                    return false;
                } else {
                    return true;
                }; 
            });
        });

        newOrder = new Order ({
            orderId: orderNum,
            userId: id,
            productId: product,
            color: uColor,
            size: uSize,
            quantity: orders,
            subTotal: subTotal
        });

        let isOrderPlaced = newOrder.save().then((order, err)=> {
            if (order) {
                return true;
            } else {
                return false;
            };
        });

        

        if (isUserUpdated && isProductUpdated && isOrderPlaced) {
            return 'Order is Placed';
        } else {
            return 'Order Failed, Contact Admin';
        }
         

        //order table
    };


    module.exports.checkEmailExists = (reqBody) => {
        return User.find({email: reqBody.email}).then(result => {
            if (result.length > 0) {
                return false;
            } else {
                return true;
            }
        })
    }


//[SECTION] Functionalities [RETRIEVE]

    module.exports.getAllUser = () => {
        return User.find({}).then(result => {
            return result;
        });
    }

    module.exports.singleUser = (id) => {
        return User.findById(id).then(user => {
            return user
        })
    }

    module.exports.getUserOrder = (id) => {

        return User.findById(id).then(user => {
            grandTotal = [];

            user.orders.forEach(item => grandTotal.push(item.subTotal))

            let sum = grandTotal.reduce((sum, x) => sum + x);
            
            console.log(sum)

            return `${user.orders}
            ${grandTotal}
            Total Amount is ${sum}`

            // Product.forEach(user.orders[0].productId => console.log(orders)
                

            // })     
        })

    }


//[SCTION] Functionality [Update]

    module.exports.setAsAdmin = (userId) => {
      let updates = {
        isAdmin: true
      }
      return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
         if (admin) {
            return true; 
         } else {
            return 'Updates Failed to implement'; 
         }
      });
   }; 

   module.exports.setAsNonAdmin = (userId) => {
     let updates = {
        isAdmin: false 
     }
     return User.findByIdAndUpdate(userId, updates).then((user, err) => {  
         if (user) {
            return true; 
         } else {
            return 'Failed to update user'; 
         };
     });
   };

