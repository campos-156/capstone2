const mongoose = require('mongoose')

const productSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true,'Name is Required']
    },
    description: {
        type: String,
        required: [true,'Description is Required']
    },
    price: {
        type: Number,
        required: [true,'Price needed for Item']
    },
    thumbnail: {
        type: String,
        default: 'https://99designs-blog.imgix.net/blog/wp-content/uploads/2017/11/Tshirt-design.jpg?auto=format&q=60&w=2060&h=1158.75&fit=crop&crop=faces'
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    },
    orders: [
        {
            orderId: {
                type: String,
                required: [true, "Order is Required"]
            },
            userId: {
                type: String,
                required: [true, "User id Required"]
            },
            color: {
                type: String,
                required: [true, "Color Required"]
            },
            size: {
                type: String,
                required: [true, "Size Required"]
            },
            quantity: {
                type: Number,
                required: [true, "Number of orders required"]
            },
            orderedOn: {
                type: Date,
                default: new Date()
            }
        }
    ]
})

module.exports = mongoose.model("Products", productSchema)