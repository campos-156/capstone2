const mongoose = require('mongoose')

const orderSchema = mongoose.Schema({
    orderId: {
        type: String,
        required: [true,'orderId is Required']
    },
    userId: {
        type: String,
        required: [true,'userId is Required']
    },
    productId: {
        type: String,
        required: [true,'productId needed for Item']
    },
    color: {
        type: String,
        required: [true, "Color Required"]
    },
    size: {
        type: String,
        required: [true, "Size Required"]
    },
    quantity: {
        type: Number,
        required: [true, 'Order quantity required']
        
    },
    subTotal: {
        type: Number,
        default: 0
    },
    isPaid: {
        type: Boolean,
        default: false
    },
    isDelivered: {
        type: Boolean,
        default: false
    },
    createdOn: {
        type: Date,
        default: new Date()
    },

})

module.exports = mongoose.model("Orders", orderSchema)