const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    firstName: {
        type: String,
        required: [true, 'First Name is Required']
    },
    lastName: {
        type: String,
        required: [true, 'Last Name is Required']
    },
    email: {
        type: String,
        required: [true, 'Email is Required']
    },
    mobile: {
        type: String,
        required: [true, 'Number is Required']
    },
    password: {
        type: String,
        required: [true, 'Password is Required']
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    orders: [
        {
        orderId: {
            type: String,
            required: [true, 'Need orderId']
        },
        productId:{
            type: String,
            required: [true, 'Product is Required']
        },
        color: {
            type: String,
            required: [true, 'Color is required']
        },
        size: {
            type: String,
            required: [true, 'Size is required']
        },
        quantity:{
            type: Number,
            required: [true, 'Need number of orders']
        },
        price: {
            type: Number,
            defaut: [true, 'Price is needed']
        },
        subTotal: {
            type: Number,
            default: 0
        },
        orderedOn:{
            type: Date,
            default: new Date()
        }
    }]

    });

//[SECTION] Model 
module.exports = mongoose.model("User", userSchema);