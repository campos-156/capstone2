const mongoose = require('mongoose')

const serviceSchema = mongoose.Schema({
    client: {
        type: String,
        required: [true, 'Client Name Required']
    },
    location: {
        type: String,
        required: [true, 'Location is Required']
    },
    projectManager: {
        type: String,
    },
    service: [
        {
            serviceName: {
                type: String,
                required: [true, "Name is required"]
            },
            teamLeader: {
                type: String,   
            },
            serviceType: {
                type: String,    
            },
            location: {
                type: String,    
            },
            workers: {
                type: String,      
            },
            status: {
                type: String,
            },
            dateCreated: {
                type: Date,
                default: new Date()
                
            }
        }
    ],
    dateCreated: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model("Services", serviceSchema)