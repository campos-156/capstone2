//[SECTION] Dependencies and Modules
    const exp = require('express');
    const controller = require('../controllers/users');
    const auth = require('../auth');

//[SECTION] Routing Component
    const route = exp.Router();

//[SECTION] Routes-[POST]
    route.post('/register', (req, res) => {
        let userData = req.body;

        controller.checkEmailExists(req.body).then(outcome => {
			if (outcome === true){
                controller.registerUser(userData).then(outcome => {
                    res.send(outcome);
                });
            } else {
                res.send(false)
            };
		});

        
    })

    route.post('/check-email', (req, res) => {
		controller.checkEmailExists(req.body).then(outcome => {
			res.send(outcome);
		});
	});

    route.post('/login', (req,res) => {
		let data = req.body;
		controller.loginUser(data).then(outcome => {
			res.send(outcome);
		})
	})

    route.post('/order', auth.verify, (req,res) => {
		let token = req.headers.authorization;
		let payload = auth.decode(token);

		let userId = payload.id
		let isAdmin = payload.isAdmin

		let productId = req.body.productId
        let orders = req.body.quantity
        let color = req.body.color
        let size = req.body.size

		let data = {
			userId: userId,
		    productId: productId,
            color: color,
            size: size,
            orders: orders,
		}

		if (!isAdmin) {
			controller.order(data).then(outcome => {
        		res.send(outcome);
        	}); 

		} else {
			res.send(`Admin can't enroll`)
		}

	})



//[SECTION] Routes-[GET]
    route.get('/all',(req,res) => {
        controller.getAllUser().then(outcome => {
            res.send(outcome)
        });
    });

    route.get('/profile', auth.verify,(req,res) => {
        let token = req.headers.authorization;
		let payload = auth.decode(token);
        let id = payload.id;

        controller.singleUser(id).then(outcome => res.send(outcome))

    })

    route.get('/:userId/order-list',auth.verify,(req,res) => {
        let token = req.headers.authorization;
		let payload = auth.decode(token);
        
        let id = payload.id;
        let checker = req.params.userId;

        if (id !== checker ) {
            return res.send('Unauthenticated User')
        } else {
            controller.getUserOrder(id).then(outcome => res.send(outcome))
        }
    })

//[SECtION] Routes-[UPDATE]
    route.put('/:userId/set-as-admin', auth.verify, (req, res) => {

		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;

   		let id = req.params.userId;
        
        if (isAdmin) {
            controller.setAsAdmin(id).then(outcome => res.send(outcome))
        } else {
            res.send('Unauthorized User')
        }
   });

   route.put('/:userId/set-as-user', auth.verify, (req, res) => {

		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;

		let id = req.params.userId;

		//isAdmin ? controller.setAsNonAdmin(id).then(result => res.send(result)) : res.send('Not Permitted')

        if (isAdmin) {
            controller.setAsNonAdmin(id).then(result => res.send(result))
        } else {
            res.send('Not Permitted')
        }
   });

    module.exports = route;