//[SECTION] Dependencies and Modules
    const exp = require('express');
    const controller = require('../controllers/services');
    const auth = require('../auth');

//[SECTION] Routing Component
    const route = exp.Router();

//[SECTION] Routes-[POST]
    route.post('/create', (req, res) => {
        let userData = req.body;
        controller.registerClient(userData).then(outcome => {
            res.send(outcome);
        });
    })




 module.exports = route;