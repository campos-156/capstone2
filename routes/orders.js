//[SECTION] Dependencies and Moduls
    const exp = require('express');
    const controller = require('../controllers/orders');
    const auth = require('../auth');

//[SECTION] Routing Component
    const route = exp.Router();

//[SSECTION]
    route.get('/all',auth.verify, (req, res) => {
            let token = req.headers.authorization;
            let payload = auth.decode(token);
            let isAdmin = payload.isAdmin;

            isAdmin ? controller.getAllOrders().then(outcome => res.send(outcome)) : res.send('UnauthorizedUser')
        });



module.exports = route;