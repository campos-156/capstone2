//[SECTION] Dependencies and Modules
    const exp = require('express');
    const controller = require('../controllers/products');
    const auth = require('../auth');

//[SECTION] Routing Component
    const route = exp.Router();

//[SECTION] Routes-[POST]

    route.post('/create', auth.verify, (req,res)=>{
        let productData = req.body;
        let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
        
        if (isAdmin) {
            controller.createProduct(productData).then(outcome => res.send(outcome))
        } else {
            res.send('Unauthorized User')
        }

    })

//[SECTION] Routes-[GET]

    route.get('/all-items',auth.verify, (req,res)=>{
        let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;
        
        if (isAdmin) {
            controller.getAllProducts().then(outcome => res.send(outcome))
        } else {
            res.send('Unauthorized User')
        }
    })

    route.get('/active', (req, res) => {
        controller.getActiveProducts().then(outcome => {
            res.send(outcome);
        });
    });

    route.get('/:productId/item-order-list', auth.verify, (req, res) => {

		let token = req.headers.authorization;
		let payload = auth.decode(token);
		let isAdmin = payload.isAdmin;

   		let id = req.params.productId;
        
        if (isAdmin) {
            controller.productOrders(id).then(outcome => res.send(outcome))
        } else {
            res.send('Unauthorized User')
        }
   });

   route.get('/:productId', (req,res) => {
       let id = req.params.productId;
       controller.singleProduct(id).then(outcome => res.send(outcome))
   })

//[SECTION] Routes-[PUT]

    route.put('/:productId/update', auth.verify, (req,res) => {
        let productId = req.params;
        let updateData = req.body;
        let token = req.headers.authorization;
        let payload = auth.decode(token);
        let isAdmin = payload.isAdmin;

        if (isAdmin) {
            controller.updateProduct(productId, updateData).then(outcome => res.send(outcome))
        } else {
            res.send(false)
        }
    })

    route.put('/:productId/archive', auth.verify, (req, res) => {
     
     let token = req.headers.authorization;
     let isAdmin = auth.decode(token).isAdmin; 

     let params = req.params; 
     
     (isAdmin) ? 
        controller.archiveProduct(params).then(result => {
        res.send(result);
        })
       : 
         res.send('Unauthorized User');

    });

    module.exports = route;