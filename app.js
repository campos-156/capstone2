//[SECTION] Dependencies and Modules
    const exp = require('express');
    const dotenv = require("dotenv");
    const mongoose = require('mongoose');
    const cors = require("cors");
    const userRoutes = require('./routes/user');
    const productsRoutes = require('./routes/products');
    const ordersRoutes = require('./routes/orders')

//[SECTION] Environment Variables
    dotenv.config();
    const port = process.env.PORT;
    const mongoDb = process.env.MONGO_STRING;

//[SECTION] Server Setups
    const app = exp();
    app.use(cors());
    app.use(exp.json()); //middleware
    app.use(exp.urlencoded({extended: true}));

//[SECTION] Database Connect
    mongoose.connect(mongoDb);
    const db = mongoose.connection;
    db.once('open', () => console.log(`Connected to Atlas [MongoDB]`));
//[SECTION] Server Routes
    app.use('/users', userRoutes);
    app.use('/products', productsRoutes);
    app.use('/orders', ordersRoutes);

//[SECTION] Server Response
    app.get('/', (req,res) => {
		res.send(`Capstone 2 E-Commerce Deployed Successfully`);
	});

    app.listen(port, () => {
        console.log(`API is now online on ${port}`)
    });